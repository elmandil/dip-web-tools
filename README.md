## Install dependencies for the frontend

`cd src/main/resources/static`

`npm install`

## Install project

In the root directory, run:

`mvn clean install`