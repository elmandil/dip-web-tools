package cern.ics.dashboard.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
@CrossOrigin(origins = "https://bcast.web.cern.ch/connect/broadcast")
public class MainWebController implements ErrorController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView renderSelectionForm() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView renderError() {
        return new ModelAndView("error");
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}