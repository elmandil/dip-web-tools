package cern.ics.dashboard.directory.controllers;

import cern.dip.Dip;
import cern.dip.DipBrowser;
import cern.dip.DipFactory;
import cern.ics.dashboard.directory.model.DipPoint;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class DipPointsProviderController {

	private static final List<String> directory = new ArrayList<>();

	@RequestMapping("/api/dip")
//	public DipUriDirectory publicationsList() {

	public List<String> publicationsList() {
		directory.clear();
		DipFactory df = Dip.create("uri-directory-harvester");
		DipBrowser browser = df.createDipBrowser();
		df.setDNSNode("dipnsgpn1,dpnsgpn2");
		directory.addAll(Arrays.asList(browser.getPublications("*")));
		List<String> dipPublicationsList = Arrays.asList((browser.getPublications("*")));
//		return new DipUriDirectory(directory);
		return dipPublicationsList;
	}

}