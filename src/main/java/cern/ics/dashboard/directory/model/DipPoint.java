package cern.ics.dashboard.directory.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Eleni Mandilara on 11/21/2017.
 */

@Entity
@Table(name = "DIPCM")
public class DipPoint {
    @Id
    @Column(name = "TOPIC")
    private String topic;

    @Column(name = "PUBLISHER")
    private String publisher;
    @Column(name = "HOST")
    private String host;

    public DipPoint(String topic, String publisher, String host) {
        this.topic = topic;
        this.publisher = publisher;
        this.host = host;
    }

    public String getTopic() {
        return topic;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getHost() {
        return host;
    }

    public DipPoint() {
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
