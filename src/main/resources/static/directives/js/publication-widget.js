/**
 * Created by Eleni Mandilara on 31/1/2018.
 */

app.directive("publicationWidget", function() {
    return {
        restrict: 'AE',
        controller: 'browserController',
        templateUrl: 'directives/templates/publication-widget.html',
        scope: {
        	widgettopic: '@',
        	widgetid: '@',
        	data: '@',
        	health: '@',
        	timestamp: '@'
		}
    }
});