/**
 * Created by Eleni Mandilara on 30/1/2018.
 */

app.directive("toolDescription", function() {
    return {
        restrict: 'AE',
        controller: 'toolsController',
        templateUrl: 'directives/templates/tool-description.html',
        scope: {
			tool: '@',
			home: '@'
		}
    }
});