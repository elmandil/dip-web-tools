/**
 * Created by Eleni Mandilara on 29/1/2018.
 */

app.directive("publishersTable", function() {
    return {
        restrict: 'AE',
        controller: 'publishersController',
        templateUrl: 'directives/templates/publishers-table.html'
    }
});