/**
 * Created by Eleni Mandilara on 31/1/2018.
 */

app.directive("impactTree", function() {
    return {
        restrict: 'AE',
        controller: 'browserController',
        templateUrl: 'directives/templates/impact-tree.html'
    }
});