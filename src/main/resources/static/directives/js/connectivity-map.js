/**
 * Created by Eleni Mandilara on 30/1/2018.
 */

app.directive("connectivityMap", function() {
    return {
        restrict: 'AE',
        controller: 'mapsController',
        templateUrl: 'directives/templates/connectivity-map.html',
        scope: {
        	map: '@'
		}
    }
});