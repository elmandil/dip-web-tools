/**
 * Created by Eleni Mandilara on 12/2/2018.
 */

app.directive("legendsGuide", function() {
    return {
        restrict: 'AE',
        controller: 'browserController',
        templateUrl: 'directives/templates/legends-guide.html'
    }
});