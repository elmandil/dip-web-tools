/**
 * Created by Eleni Mandilara on 29/1/2018.
 */

app.directive("ddnsMonitor", function() {
    return {
        restrict: 'AE',
        controller: 'toolsController',
        templateUrl: 'directives/templates/ddns-monitor.html'
    }
});