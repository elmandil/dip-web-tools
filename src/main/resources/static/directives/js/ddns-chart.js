/**
 * Created by Eleni Mandilara on 29/1/2018.
 */

app.directive("ddnsChart", function() {
    return {
        restrict: 'AE',
        controller: 'ddnsController',
        templateUrl: 'directives/templates/ddns-chart.html'
    }
});