/**
 * Created by Eleni Mandilara on 29/1/2018
 */

app.directive("navigationBar", function() {
    return {
        restrict: 'AE',
        templateUrl: 'directives/templates/navigation-bar.html',
        controller: 'toolsController'
    }
});