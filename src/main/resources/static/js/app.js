var app = angular.module('app', ['ngRoute', 'highcharts-ng']);

app.filter('trusted', ['$sce', function ($sce) {
    return $sce.trustAsResourceUrl;
}]);

app.config(function ($routeProvider) {
    $routeProvider
    	.when('/tools/maps/:map', {
			templateUrl: function (routeParams){ return '/partials/maps.html'; },
			controller: 'mapsController',
			access: {restricted: false},
			title: "Maps"
		})
		.when('/tools/browser', {
			templateUrl: function (routeParams){ return '/partials/browser.html'; },
			controller: 'browserController',
			access: {restricted: false},
			title: "Browser"
        })
		.when('/tools/:tool', {
			templateUrl: function (routeParams){ return '/partials/' + routeParams.tool + '.html'; },
			controller: 'toolsController',
			access: {restricted: false}
		})
		.when('/tools', {
			templateUrl: 'partials/home.html',
			controller: 'toolsController',
			access: {restricted: false},
			title: "Home"
		})
		.otherwise({
			redirectTo: '/tools'
		});
});

app.run(['$rootScope', function($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
}]);