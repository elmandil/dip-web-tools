app.controller('toolsController', function($scope) {

	$scope.tool;
	console.log("Selected tool:", $scope.tool);

	$scope.toolsAvailability = [
	{
		"tool": "publishers",
		"availability": false
	},
	{
		"tool": "maps",
		"availability": false
	},
	{
		"tool": "ddns",
		"availability": false
	},
	{
		"tool": "browser",
		"availability": true
	}];

	$scope.isToolAvailable = function(tool){
		for(i=0; i<$scope.toolsAvailability.length;i++){
			if(tool===$scope.toolsAvailability[i].tool){
				return $scope.toolsAvailability[i].availability;
			}
		}
	}

	switch ($scope.tool){
		case "publishers":
			$scope.toolName = "DIP Publishers Monitor";
			$scope.toolDescription = "The DIP Publishers Monitor is a monitoring tool that aggregates information about publishers in DIP (publisher name, PID, current state, host, total number of services and DIM version), and also visualizes all the connections of each publisher as well as its availability history."
			$scope.collapseId = "collapsePublishers";
			break;
		case "maps":
			$scope.toolName = "DIP Connectivity Maps";
			$scope.toolDescription = "This tool provides map visualizations all connections of a subset of publishers in DIP. They also provide the related information about each of these publishers (publisher name, PID, current state, host, total number of services and DIM version), as well as the availability history of it. Currently, we are providing the connectivity map of the publishers of ATLAS. To request a map for your DIP publishers, please contact icecontros.support@cern.ch"
			$scope.collapseId = "collapseMaps";
			break;
		case "ddns":
			$scope.toolName = "DIP Namespace Monitor";
			$scope.toolDescription = "The DIP Namespace Monitor tool provides visualizations about the CPU and memory usage, the number of publications, clients and servers of DIP DNS."
			$scope.collapseId = "collapseDDNS";
			break;
		case "browser":
			$scope.toolName = "DIP Browser";
			$scope.toolDescription = "This tool provides an easy way to access the data of publications in DIP."
			$scope.collapseId = "collapseBrowser";
			break;
	}
});