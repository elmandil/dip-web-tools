app.controller('browserController', function($scope, $http, $log) {

	$scope.isToolAvailable = true;
	$scope.broadcastSubscriptions = "";

	$scope.testMode = true;

	$scope.testValue1 = 0;
	$scope.testValue2 = 0;
	$scope.testValue3 = 0;
	$scope.testPubName = "dip://dip/pub1";
	$scope.testHealth = true;

	$scope.impactTreeSectionName = "IMPACT Tree";
	$scope.publicationWidgetSectionName = "Data visualizations";
	$scope.directoryUrl = "/api/dip";
	$scope.dipTopics = [];
	$scope.atmourl = "https://bcast.web.cern.ch/connect/broadcast";
	$scope.publicationData = {};
	$scope.subscriptions = [];


	$scope.parseData = function(data){
		var parsedData = {
			"name": data.name,
			"health": data.valid,
			"timestamp": new Date(data.timestamp),
			"data": []
		}
        return parsedData;
	}

	$scope.renderData = function(event, publicationData){
		$log.info("Received data:", publicationData);

		var keysNumber = Object.keys(publicationData).length;

		if(keysNumber>2){
			$scope.publicationExists = false;
			publicationData = $scope.parseData(publicationData);
			for(i=0;i<$scope.subscriptions.length;i++){
				if($scope.subscriptions[i].name === publicationData.name) {
					$scope.subscriptions[i] = publicationData;
					$scope.publicationExists = true;
					break;
				}
			}
			if(!$scope.publicationExists){
				$scope.subscriptions.push(publicationData);
			}
		}

		$log.warn("Rendering data:", $scope.subscriptions);
   	}

	$scope.$on('atmoUpdate', $scope.renderData);

	// Test
	// Function to dispatch mock data
	$scope.mockDataDispatcher = function(){
		var mockDipData = { "name": $scope.testPubName,
						"valid": $scope.testHealth,
						"timestamp": 55,
						"data": [{ "field": "horizontalPos", "value": $scope.testValue1},
								{ "field": "verticalPos", "value": $scope.testValue2},
								{ "field": "zPos", "value": $scope.testValue3}]
						};

		$scope.$emit('atmoUpdate', mockDipData);
		console.log("Dispatching mock data...", mockDipData);
	}


	// Clean all special characters except numbers and letters
	$scope.trim = function(str){
		return str.replace(/[^a-zA-Z0-9]/g,'_');
	}

	$scope.sortType = 'name';
    $scope.sortReverse = false;
	$scope.dipData = [];

	// $scope.mockDipTopics = ["dip://dip/cmw/gateway/rda2dip/setting", "dip/cmw/gateway/rda2dip/watchdog1500", "dip/cmw/gateway/rda2dip/expertSetting", "dip/acc/LHC/Beam/BPM/LSS1R/B2_DOROS"];

	// Render impact tree
	// TODO: Render into tree structure
	$scope.renderImpactTree = function(response){
		$scope.dipTopics = response.data;
		console.log("success, received DIP topics:", $scope.dipTopics);
	}

	$scope.onError = function(reason){
		console.log("Request was unsuccessful, with reason:", reason);
	}

	// Dispatch and AJAX request to get the DIP publications list from the URI directory
	$scope.requestDipTopics = function(directoryUrl){
		console.log("requesting dip topics...");
		$http({
    		method: 'GET',
    		url: directoryUrl
    	}).then(
    		// If request was successful, render tree
    		$scope.renderImpactTree
    	).catch(function (reason) {
    		// Otherwise, inform about the error
    		$scope.onError
    	});
	}

	$scope.requestDipTopics($scope.directoryUrl);

    $scope.displayData = function(dipTopic){
    	if($scope.broadcastSubscriptions.indexOf(dipTopic) !== -1) {
			console.log("Subscription already exists, nothing to do.")
		} else {

			// Concatenate "dip://" prefix in order to subscribe to DIP publication
			var topicToSubscribe = "dip://" + dipTopic;

			// Subscribe broadcast to DIP publication
			$scope.broadcastSubscriptions = $scope.broadcastSubscriptions + ";" + topicToSubscribe;

			console.log("Adding subscription to", topicToSubscribe);
			$scope.atmoSubSocket.push(JSON.stringify({add:[topicToSubscribe]}));

		}
	}


	$scope.initializeAtmosphere = function(requestSpecs) {

		var socket = atmosphere;
		var request = new atmosphere.AtmosphereRequest();
		request.url = requestSpecs.url;
		request.contentType = requestSpecs.contentType;
		request.transport = requestSpecs.transport;
		request.fallbackTransport = requestSpecs.fallbackTransport;

		request.onOpen = function(response){
			console.log('Atmosphere connected using ' + response.transport);
//			subSocket.push(JSON.stringify({add:["dip://dip/CMS/GCS/"]}));
		}

		request.onMessage = function (response) {
			console.log('Atmosphere received : '+response.responseBody);
			try {
				var atmoData = JSON.parse(response.responseBody);
				$scope.$emit('atmoUpdate', atmoData);
				console.log('Dispatched atmo data:', atmoData);
			} catch (e) {
				console.log('Error: ', response.responseBody);
				return;
			}
        };

        request.onError = function(response) {
        	console.log('Atmosphere error :', response.responseBody);
        };

        subSocket = socket.subscribe(request);
        return subSocket;
	};

	$scope.requestSpecs = {
		"url": $scope.atmourl,
		"contentType": "application/json",
		"transport": "websocket",
		"fallbackTransport": "long-polling"
	}

	$scope.atmoSubSocket = $scope.initializeAtmosphere($scope.requestSpecs);
});