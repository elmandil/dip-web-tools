app.controller('publishersController', function($scope) {

	$scope.isToolAvailable = false;

	$scope.publisherTableHeaders = ["Name", "PID", "State", "Host", "Total services", "DIM version"];

	$scope.publisherTableData = [
		{
			"publisherName": "-8917@cs-ccr-bbif" ,
			"pid": 372,
			"state": 'AVAILABLE',
			"host": "cs-ccr-vacm01",
			"totalServices": 42,
			"dimVersion": 1937
		},
		{
			"publisherName": "108917@cs-ccr-bbif" ,
			"pid": 377,
			"state": 'AVAILABLE',
			"host": "cs-ccr-vacm01",
			"totalServices": 420,
			"dimVersion": 23
        }
	];

});