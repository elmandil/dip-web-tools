app.controller('mapsController', function($scope, $routeParams) {

	$scope.isToolAvailable = false;

	$scope.selectionMade = false;
	$scope.mapSelected = $routeParams.map;
	$scope.connectivityMaps = ["ALICE", "ATLAS", "CMS", "LHCb"];
	if($scope.mapSelected) {
		$scope.selectionMade = true;
	}
	$scope.map = $scope.mapSelected;

	$scope.tableHeaders = ["Name", "PID", "State", "Host", "Total services", "DIM version"];

	$scope.tableData = [{
		"publisherName": "ATLAS-88sdfjf@cs-ccr-gcsal" ,
		"pid": 372,
		"state": 'AVAILABLE',
		"host": "cs-ccr-vacm01",
		"totalServices": 42,
		"dimVersion": 1937
	}];


	$scope.mapChart = {
		n: 'publishersAvailabilityChart',
		cfg: {
			options: {
				chart: {
					type: 'line'
				},
				legend: ''
			},
			series: [{ data: [0, 0, 0, 2, 2, 0, 2, 2, 2, 0, 1, 0, 1, 1, 2, 1, 0, 0, 0, 1, 1, 2, 1, 2, 1, 2, 1, 0, 1, 2, 0, 1, 2, 0, 1, 2, 1], color: "tomato", step: "true"}],
			title: { text: 'Publisher\'s Availability Chart' },
			xAxis: { title: { text: 'Date (CET)' } },
			yAxis: { title: { text: 'Availability Range' }, categories: ['Unknown', 'Unavailable', 'Available']},
		}
	};

	var diameter = 840,
		radius = diameter / 2,
		innerRadius = radius - 120;

	var cluster = d3.cluster()
		.size([690, innerRadius]);

	var line = d3.radialLine()
		.curve(d3.curveBundle.beta(0.85))
		.radius(function(d) { return d.y; })
		.angle(function(d) { return d.x / 180 * Math.PI; });


	var svg = d3.select("#mapContainer").append("svg")
		.attr("width", diameter)
		.attr("height", diameter)
	  	.append("g")
		.attr("transform", "translate(" + radius + "," + radius + ")");

	var link = svg.append("g").selectAll(".link"),
		node = svg.append("g").selectAll(".node");

	d3.json("flare.json", function(error, classes) {
	  	if (error) throw error;

	  	var root = packageHierarchy(classes)
			.sum(function(d) { return d.size; });

		cluster(root);

		link = link
			.data(packageImports(root.leaves()))
			.enter().append("path")
			.each(function(d) { d.source = d[0], d.target = d[d.length - 1]; })
			.attr("class", "link")
			.attr("d", line);

		node = node
			.data(root.leaves())
			.enter().append("text")
			.attr("class", "node")
			.attr("dy", "0.31em")
			.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (d.y + 8) + ",0)" + (d.x < 180 ? "" : "rotate(180)"); })
			.attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
			.text(function(d) { return d.data.key; })
			.on("mouseover", mouseovered)
			.on("mouseout", mouseouted);
	});
});