app.controller('ddnsController', function($scope) {

	$scope.isToolAvailable = false;

	$scope.ddnsTableHeaders = ["Active name server", "Active since", "Services", "Servers", "Clients"];
	$scope.ddnsTableData = ["Primary", "2016.12.06 12:41", 2342, "432/215/117", 415];

	$scope.items = [
		{
			n: 'cpuUsage',
			cfg: {
				options: {
					chart: {
						type: 'column'
					},
					legend: ''
				},
				series: [{ data: [1, 4, 8, 3, 2], color: "lavender"}],
				title: { text: 'CPU Usage' },
				xAxis: { categories: []},
				yAxis: { title: { text: 'CPU Usage (%)' }},
			}
		},
		{
			n: 'cpuUsage1',
			cfg: {
				options: {
					chart: {
						type: 'gauge'
					},
					legend: ''
				},
				series: [{ data: [80], color: "lavender"}],
				title: { text: 'CPU Usage1' },
				xAxis: { categories: []},
				yAxis: {
					min: 0,
					max: 200,

					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',

					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
				 	title: {
						text: 'km/h'
					},
					plotBands: [{
						from: 0,
						to: 120,
						color: '#55BF3B' // green
					}, {
						from: 120,
						to: 160,
						color: '#DDDF0D' // yellow
					}, {
						from: 160,
						to: 200,
						color: '#DF5353' // red
					}]
				}
        	}
        },
		{
			n: 'memoryUsage',
			cfg: {
				options: {
					chart: {
						type: 'column'
					},
					legend: ''
				},
				series: [{ data: [4, 2, 3, 2, 1], color: "tomato"}],
				title: { text: 'Memory Usage' },
				xAxis: { categories: []},
				yAxis: { title: { text: 'Memory Usage (kB)' }},
			}
		},
		{
			n: 'publicationsNumber',
			cfg: {
				options: {
					chart: {
						type: 'line'
					},
					legend: ''
				},
				series: [{ data: [1, 4, 3, 2, 6], color: "tomato"}],
				title: { text: 'Publications Number' },
				xAxis: { categories: []},
				yAxis: { title: { text: 'Publications number' } },
			}
		},
		{
			n: 'clientsNumber',
			cfg: {
				options: {
					chart: {
						type: 'line'
					},
					legend: ''
				},
				series: [{ data: [1, 4, 3, 2, 6]}],
				title: { text: 'Clients Number' },
				xAxis: { categories: []},
				yAxis: { title: { text: 'Clients number' } },
			}
		},
		{
			n: 'serversNumber',
			cfg: {
				options: {
					chart: {
						type: 'line'
					},
					legend: ''
				},
				series: [{ data: [11, 43, 47, 78, 12, 98, 4, 3, 2, 6]}],
				title: { text: 'Servers Number' },
				xAxis: { categories: []},
				yAxis: { title: { text: 'Servers number' } },
			}
		}
	];

});