<!DOCTYPE html>

<html lang="en" ng-app='app'>

	<head>
		<meta charset="UTF-8">
		<title ng-bind="$root.title + ' · DIP Tools'">DIP Tools</title>
		<link href = "images/dip.png" rel="icon" type="image/gif">

		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="node_modules/angular/angular.min.js" type="text/javascript"></script>
		<script src="node_modules/angular-route/angular-route.min.js" type="text/javascript"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
		<script src="https://npmcdn.com/packery@2.0.0/dist/packery.pkgd.min.js" type="text/javascript"></script>
		<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

		<script src="js/app.js" type="text/javascript"></script>

		<!-- Import controllers -->
		<script src="js/controllers/toolsController.js"></script>
		<script src="js/controllers/ddnsController.js"></script>
		<script src="js/controllers/publishersController.js"></script>
		<script src="js/controllers/mapsController.js"></script>
		<script src="js/controllers/browserController.js"></script>

		<!-- Import directives -->
		<script src="directives/js/navigation-bar.js"></script>
		<script src="directives/js/ddns-chart.js"></script>
		<script src="directives/js/publishers-table.js"></script>
		<script src="directives/js/tool-description.js"></script>
		<script src="directives/js/impact-tree.js"></script>
		<script src="directives/js/publication-widget.js"></script>
		<script src="directives/js/legends-guide.js"></script>

		<link href="stylesheets/css/app.css" rel="stylesheet">
		<link href="stylesheets/css/browser.css" rel="stylesheet">
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.5/lodash.core.js"></script>

		<!-- Resources for publishers monitor and charts -->
		<link href="stylesheets/css/publishers.css" rel="stylesheet">
		<script src="js/publishers.js"></script>
		<script src="http://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/highcharts-more.js"></script>
		<script src="js/highcharts-ng.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/atmosphere/2.2.9/atmosphere.js"></script>

	</head>

	<body>
		<div class="container-fluid">
			<navigation-bar></navigation-bar>
			<div ng-view></div>
		</div>
	</body>

</html>